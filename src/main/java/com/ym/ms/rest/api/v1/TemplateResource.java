package com.ym.ms.rest.api.v1;

import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.google.common.collect.Lists;

import com.ym.ms.annotate.NonRemoteInfoAnnotate;
import com.ym.ms.dto.*;
import com.ym.ms.entity.Template;
import com.ym.ms.utils.HeaderUtils;
import com.ym.ms.utils.RestTemplateClient;
import com.ym.ms.commons.enums.InsTypeEnum;
import com.ym.ms.dto.file.FileQueryParamDto;
import com.ym.ms.dto.file.FileQueryResultDto;
import com.ym.ms.service.ITemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

/**
 * 微服务APIS
 *
 * @author zl
 * @date 9.5
 */
@Api(tags = "模板")
@RestController
@RequestMapping("/api/v1")
public class TemplateResource extends ApiController {

    @Autowired
    private ITemplateService templateService;
    @Autowired
    private RestTemplateClient restTemplateClient;

    @PostMapping
    public R insert(@RequestBody Template param) {
        return success(templateService.save(param));
    }

    @ApiOperation(value = "模板查询", tags = {"模板"}, notes = "模板查询")
    @NonRemoteInfoAnnotate
    @RequestMapping(value = "/queryPage")
    public PageResult<TemplateResultDto> queryPage() {
        return templateService.queryPage();
    }

    @ApiOperation(value = "模板查询", tags = {"模板"}, notes = "模板查询")
    @RequestMapping(value = "/query-one", method = {RequestMethod.GET})
    public Result<Template> queryOne(@RequestParam Long id) {
        return templateService.queryOne(id);
    }

    @ApiOperation(value = "模板查询", tags = {"模板"}, notes = "模板查询")
    @RequestMapping(value = "/query-onedto", method = {RequestMethod.GET})
    public Result<TemplateResultDto> queryOneDto(@RequestParam Long id) {
        return templateService.queryOneDto(id);
    }

    @ApiOperation(value = "远程调用", tags = {"模板"}, notes = "远程调用")
    @PostMapping("/doPost")
    public PageResult doPost(@ApiParam(name = "remoteInfo", value = "调用方信息 (Encode json格式):{\"account\":\"admin\",\"accountName\":\"张三\",\"accountUuid\":\"0dfb9cf5ff1c49c387b2f5d8292a8975\",<br>\"insTypeCode\":\"0\",\"insTypeName\":\"平台\",<br>\"insId\":\"1001\",\"insName\":\"智慧运输平台\"<br>,\"appId\":\"appId123\"}") @RequestHeader("remoteInfo") String remoteInfo,
                             @ApiParam(name = "paramDto", value = "查询参数") @RequestBody @Valid FileQueryParamDto param) {
        // 设备档案微服务-批量创建设备档案
        HeaderInfo hi = new HeaderInfo();
        hi.setAccount("1");
        hi.setAccountName("2");
        hi.setAccountUuid("3");
        hi.setAppId(RequestContext.getRemoteInfo().getHi().getAppId());
        hi.setInsId("5");
        hi.setInsName("6");
        hi.setInsTypeName("7");
        PageResult<FileQueryResultDto> result = null;
        try {
            result = restTemplateClient.httpPost("http://127.0.0.1:8081/api/v1/find",
                    HeaderUtils.getHeader(hi), param, new ParameterizedTypeReference<PageResult<FileQueryResultDto>>() {
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    @ApiOperation(value = "不验证header RemoteInfo", tags = {"模板"}, notes = "远程调用")
    @PostMapping("/doPostNon")
    @NonRemoteInfoAnnotate
    public PageResult doPostNon(
            @ApiParam(name = "paramDto", value = "查询参数") @RequestBody @Valid FileQueryParamDto param) {

        TemplateResultDto dto = new TemplateResultDto();
        dto.setId(1);

        List<TemplateResultDto> dtos = Lists.newArrayList();
        KvInfo k = new KvInfo();
        k.setKey(String.valueOf(InsTypeEnum.MANU.getKey()));
        k.setVal(InsTypeEnum.MANU.getDescription());


        List l = Lists.newArrayList();
        l.add(k);
        dto.setInsType(l);


        dtos.add(dto);
        PageResult<TemplateResultDto> result;

        result = PageResult.ok();
        result.setRows(dtos);
        return result;
    }

}
