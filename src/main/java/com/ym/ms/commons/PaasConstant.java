package com.ym.ms.commons;

/**
 * 常量信息
 *
 * @author zl
 * @date 9.5
 */
public class PaasConstant {

    /**
     * 是否: 1是
     */
    public static final Integer YES = 1;
    /**
     * 是否: 0否
     */
    public static final Integer NO = 0;
    /**
     * 系统用户名
     */
    public static String SUCCESS = "000000";
    /**
     * 最大页码
     */
    public static final int MAX_PAGE_NUM = 999999;
    /**
     * 默认字符编码
     */
    public static final String DEFAULT_ENCODING = "UTF-8";

    /**
     * 服务名
     */
    public static final String SVC_NAME = "t_m";
}
