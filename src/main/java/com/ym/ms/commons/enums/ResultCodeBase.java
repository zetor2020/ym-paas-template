package com.ym.ms.commons.enums;


/**
 * 响应码枚举，参考HTTP状态码的语义
 *
 * @author zl
 * @date 9.5
 */
public enum ResultCodeBase implements IResultCode {
    /*
     * 系统异常
     */
    SUCCESS("000000", "success"),
    RE_SUCCESS("100000", "resuccess"),
    /*
     * 通用异常
     */
    C0000001("C0000001", "解析header错误"),
    C0000002("C0000002", "系统内部异常"),// DB相关
    C0000003("C0000003", "系统内部异常"),// DB约束相关
    C0000004("C0000004", "{0}"),// 系统自定义异常
    C0000005("C0000005", "系统内部异常"),// 后台异常
    C0000006("C0000006", "系统内部异常"),// 参数解析异常
    C0000007("C0000007", "系统内部异常"),// Servlet请求异常
    BIZ00001("BIZ00001", "{0}"); // 业务自定义异常

    ResultCodeBase(String code, String msg) {
        this.code = code;
        this.message = msg;
    }

    private String code;

    private String message;

    @Override
    public String getCode() {
        return code;
    }

    private void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    private void setMessage(String msg) {
        this.message = msg;
    }
}