package com.ym.ms.commons.enums;

/**
 * @author zl
 */
public interface IResultCode {

    String getCode();

    String getMessage();
}
