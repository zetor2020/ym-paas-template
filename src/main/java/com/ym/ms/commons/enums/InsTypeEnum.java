package com.ym.ms.commons.enums;

/**
 * 组织类型枚举
 *
 * @author zl
 * @date 9.5
 */
public enum InsTypeEnum {
    PLAT(0, "平台"),
    MANU(1, "供应商"),
    DISTR(2, "分销商");

    private int key;
    private String description;

    InsTypeEnum(int key, String description) {
        this.key = key;
        this.description = description;
    }

    public static InsTypeEnum get(int key) {
        InsTypeEnum re = null;
        for (InsTypeEnum e : InsTypeEnum.values()) {
            if (e.getKey() == key) {
                re = e;
                break;
            }
        }
        return re;
    }

    public int getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

}
