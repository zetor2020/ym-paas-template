package com.ym.ms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ym.ms.dto.TemplateResultDto;
import com.ym.ms.entity.Template;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * Mapper 接口
 *
 * @author zl
 * @date 9.5
 */
public interface ITemplateMapper extends BaseMapper<Template> {

    @Select("select t.id, t.create_time, t.update_time from test t where t.id = #{id}")
    TemplateResultDto selectOneDto(@Param("id") Long id);
}