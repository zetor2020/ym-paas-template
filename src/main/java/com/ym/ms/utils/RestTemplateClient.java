package com.ym.ms.utils;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author zl
 * @version 1.0.0
 * @date 2018/3/13 14:08
 */
public class RestTemplateClient {

    private RestTemplate restTemplate;

    public RestTemplateClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public <T> T httpRequest(String url, HttpMethod httpMethod, Map<String, String> token, Object body, ParameterizedTypeReference<T> reference) {
        HttpHeaders headers = new HttpHeaders();
        if (token != null) {
            for (Map.Entry<String, String> entry : token.entrySet()) {
                headers.set(entry.getKey(), entry.getValue());
            }
        }
        Map map = CommonObjUtil.java2Map(body);
        url = CommonObjUtil.expandURL(url, map.keySet());
        HttpEntity<Object> entity = new HttpEntity<Object>(body, headers);
        HttpEntity<T> response = restTemplate.exchange(url, httpMethod, entity, reference, map);
        return response.getBody();
    }

    public <T> T httpRequest(String url, MediaType mediaType, HttpMethod method, Map<String, String> token, Object data, ParameterizedTypeReference<T> typeRef) {
        HttpHeaders headers = new HttpHeaders();
        if (mediaType != null) {
            headers.setContentType(mediaType);
        }
        if (token != null) {
            for (Map.Entry<String, String> entry : token.entrySet()) {
                headers.set(entry.getKey(), entry.getValue());
            }
        }
        HttpEntity<Object> entity = new HttpEntity<Object>(data, headers);

        if (method == HttpMethod.GET) {
            Map<String, Object> map = CommonObjUtil.java2Map(data);
            url = CommonObjUtil.expandURL(url, map.keySet());
            return restTemplate.exchange(url, HttpMethod.GET, entity, typeRef, map).getBody();
        }
        HttpEntity<T> response = restTemplate.exchange(url, method, entity, typeRef);
        return response.getBody();
    }

    public <T> T httpGet(String url, Map<String, String> token, Object data, ParameterizedTypeReference<T> typeRef) {
        return httpRequest(url, null, HttpMethod.GET, token, data, typeRef);
    }

    public <T> T httpPost(String url, Map<String, String> token, Object data, ParameterizedTypeReference<T> typeRef) {
        return httpRequest(url, MediaType.APPLICATION_JSON, HttpMethod.POST, token, data, typeRef);
    }


    public <T> T httpPut(String url, Map<String, String> token, Object data, ParameterizedTypeReference<T> typeRef) {
        return httpRequest(url, MediaType.APPLICATION_JSON, HttpMethod.PUT, token, data, typeRef);
    }

    public Map<String, String> getTokenMap(String key, String value) {
        Map<String, String> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

}
