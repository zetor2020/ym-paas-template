package com.ym.ms.utils;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

/**
 * 字符串工具类
 *
 * @author zl
 * @version 1.0.0
 * @date 2018/8/16 16:40
 */
public class StringUtil {

    /**
     * 字符串解析，替换参数
     *
     * @param message 取值范围为{0}~{1}
     * @param param   100,200
     * @return 字符串后解析
     */
    public static String parseMessage(String message, Object... param) {
        try {
            String msg = message;
            int startIndex = message.indexOf("{");
            int endIndex = message.indexOf("}");
            int index = 0;
            while (startIndex >= 0 && endIndex > 0) {
                if (param[index] == null) {
                    param[index] = "空指针异常";
                }
                msg = msg.replace("{" + index + "}", param[index].toString());
                index++;
                startIndex = msg.indexOf("{");
                endIndex = msg.indexOf("}");
            }
            return msg;
        } catch (Exception e) {
            return message;
        }
    }

    /**
     * 截取子字符串 根据起始、结束字符串
     *
     * @param str
     * @param strStart
     * @param strEnd
     * @return
     */
    public static String parseSubStr(String str, String strStart, String strEnd) {

        /* 找出指定的2个字符在 该字符串里面的 位置 */
        int strStartIndex = str.indexOf(strStart);
        int strEndIndex = str.indexOf(strEnd);

        /* index 为负数 即表示该字符串中 没有该字符 */
        if (strStartIndex < 0) {
            System.out.println("字符串 :---->" + str + "<---- 中不存在 " + strStart + ", 无法截取目标字符串");
            return null;
        }
        if (strEndIndex < 0) {
            System.out.println("字符串 :---->" + str + "<---- 中不存在 " + strEnd + ", 无法截取目标字符串");
            return null;
        }
        /* 开始截取 */
        String result = str.substring(strStartIndex, strEndIndex).substring(strStart.length());
        return result;
    }

    /**
     * 字符串 是否为Integer数字
     *
     * @param str
     * @return
     */
    public static boolean isInteger(String str) {
        Pattern pattern = compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    /**
     * 判断数组中是否有重复值
     *
     * @param array
     * @return true:有重复 false:无重复
     */
    public static boolean checkRepeat(String[] array) {
        Set<String> set = new HashSet<>();
        for (String str : array) {
            set.add(str);
        }
        //有重复
        if (set.size() != array.length) {
            return true;
        } else {
            //不重复
            return false;
        }
    }

    public static void main(String ss[]) {
        String s = parseMessage("取值范围为{0}~{1}", 100, 200);
        System.out.println(s);
    }
}
