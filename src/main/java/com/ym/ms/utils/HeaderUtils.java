package com.ym.ms.utils;

import com.alibaba.fastjson.JSON;
import com.ym.ms.dto.HeaderInfo;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * com.ym.speaker.dto.token.Token
 *
 * @author zl
 * @version 1.0.0
 * @date 2018/9/15 15:11
 */
public class HeaderUtils {

    public static Map<String, String> getHeader(HeaderInfo headerInfo) throws Exception {
        Map<String, String> map = new HashMap<>();
        String remoteInfoJSON = "{}";
        remoteInfoJSON = URLEncoder.encode(JSON.toJSONString(headerInfo), "UTF-8");
        map.put("remoteInfo", remoteInfoJSON);
        return map;
    }

    public static Map<String, String> getFileHeader(HeaderInfo headerInfo) throws Exception {
        Map<String, String> map = getHeader(headerInfo);
        map.put("accessToken", "1");
        return map;
    }
}
