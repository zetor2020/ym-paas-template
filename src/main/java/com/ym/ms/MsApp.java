package com.ym.ms;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

/**
 * 启动服务
 */
@SpringBootApplication
@ServletComponentScan
@MapperScan("com.ym.ms.mapper")
public class MsApp {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(MsApp.class);
        app.run(args);
        System.out.println("started");
    }
}
