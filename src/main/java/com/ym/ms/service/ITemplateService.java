package com.ym.ms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ym.ms.dto.PageResult;
import com.ym.ms.dto.Result;
import com.ym.ms.dto.TemplateResultDto;
import com.ym.ms.entity.Template;

/**
 * 服务类
 *
 * @author zl
 * @date 9.5
 */
public interface ITemplateService extends IService<Template> {
    PageResult<TemplateResultDto> queryPage();

    Result<Template> queryOne(Long id);

    Result<TemplateResultDto> queryOneDto(Long id);
}
