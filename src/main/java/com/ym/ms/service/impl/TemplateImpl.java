package com.ym.ms.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ym.ms.dto.PageResult;
import com.ym.ms.dto.Result;
import com.ym.ms.dto.TemplateResultDto;
import com.ym.ms.entity.Template;
import com.ym.ms.mapper.ITemplateMapper;
import com.ym.ms.service.ITemplateService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 服务实现类
 *
 * @author zl
 * @date 9.5
 */
@Service
public class TemplateImpl extends ServiceImpl<ITemplateMapper, Template> implements ITemplateService {
    @Resource
    ITemplateMapper thisMapper;

    @Override
    public Result<TemplateResultDto> queryOneDto(Long id) {
        TemplateResultDto template = thisMapper.selectOneDto(id);
        return Result.ok(template);
    }

    @Override
    public Result<Template> queryOne(Long id) {
        QueryWrapper<Template> queryWrapper = new QueryWrapper<Template>();
        queryWrapper.lambda().eq(Template::getId, id);
        Template template = thisMapper.selectOne(queryWrapper);
        return Result.ok(template);
    }

    @Override
    public PageResult<TemplateResultDto> queryPage() {
        QueryWrapper qw = new QueryWrapper();
        qw.eq("is_del", 0);
        Page<Template> page = new Page(0, 999);
//        List<Template> rets = templateMapper.selectList(qw);
        IPage<Template> pageResult = thisMapper.selectPage(page, qw);
        // 查询结果赋值
        List<TemplateResultDto> list = new ArrayList<>();
        for (Template t : pageResult.getRecords()) {
            TemplateResultDto rid = new TemplateResultDto();
            BeanUtils.copyProperties(t, rid);
            list.add(rid);
        }
        PageResult<TemplateResultDto> pr = PageResult.ok();
        pr.setTotal(pageResult.getTotal());
        pr.setRows(list);
        return pr;
    }
}
