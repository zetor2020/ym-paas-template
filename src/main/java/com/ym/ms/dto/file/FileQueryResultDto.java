package com.ym.ms.dto.file;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * com.ym.filems.dto.query.FileQueryResultDto
 * 文件检索结果
 * @author 王春宇
 * @version 1.0.0
 * @date 2018/8/15 10:10
 */
@Data
public class FileQueryResultDto implements Serializable {
    private static final long serialVersionUID = 6598049086640555226L;

    /**
     * 主键
     */
    @JsonIgnore
    private Integer id;

    private String appId;

    private String fileId;

    private String frealName;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date fuploadTime;

    private Long furlExpireSec;

    private Integer flifeExpire;

    @Range(min = 0, max = 1, message="文件权限取值范围必须为（0公有、1私有）")
    private Integer fuseScop;

    private String remark;

}
