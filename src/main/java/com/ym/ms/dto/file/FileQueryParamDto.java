package com.ym.ms.dto.file;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ym.ms.dto.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * com.ym.filems.dto.query.FileQueryParamDto
 * 文件检索参数
 * @author 王春宇
 * @version 1.0.0
 * @date 2018/8/15 10:03
 */
@Data
public class FileQueryParamDto extends PageRequest implements Serializable {
    private static final long serialVersionUID = -5725347167043517429L;

    @ApiModelProperty(required = false, value = "文件Id，最大长度50", example = "1")
    @Length(max=50,message="文件Id最大长度50")
    @NotBlank
    private String fileId;

    @ApiModelProperty(required = false, value = "真实文件名，最大长度100", example = "100")
    @Length(max=100,message="真实文件名最大长度100")
    private String frealName;

    @ApiModelProperty(required = false, value = "文件上传开始时间",example = "2018-8-22 14:45:40")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private String beginTime;

    @ApiModelProperty(required = false, value = "文件上传结束时间",example = "2018-12-26 14:45:40")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private String endTime;

}
