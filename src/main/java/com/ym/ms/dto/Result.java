package com.ym.ms.dto;


import com.ym.ms.commons.enums.IResultCode;
import com.ym.ms.commons.enums.ResultCodeBase;
import com.ym.ms.utils.StringUtil;

import java.io.Serializable;

/**
 * 统一API响应结果封装
 *
 * @author zl
 * @date 9.5
 */
public class Result<T> implements Serializable {
	private static final long serialVersionUID = -8598958602264325171L;
	private static final String DEFAULT_SUCCESS_MESSAGE = "SUCCESS";

	private String code;
	private String message;
	private T data;
	private String svc;

	public Result<T> setCode(IResultCode resultCode) {
		this.code = resultCode.getCode();
		String serName  = System.getProperty("server.name");
		this.svc = serName;
		return this;
	}

	private Result(T data) {
		this.data = data;
	}
	private Result() {
		
	}
	
	/**
	 * @Description: 成功的返回结果
	 */
	public static <T> Result<T> ok() {
		return new Result<T>().setCode(ResultCodeBase.SUCCESS).setMessage(DEFAULT_SUCCESS_MESSAGE);
	}

	/**
	 * @Description: 成功的返回结果
	 */
	public static <T> Result<T> ok(T data) {
		return new Result<T>(data).setCode(ResultCodeBase.SUCCESS).setMessage(DEFAULT_SUCCESS_MESSAGE);
	}
	
	/**
	 * 失败
	 */
	public static <T> Result<T> err(IResultCode resultCode) {
		String msg = StringUtil.parseMessage(resultCode.getMessage(), "", "", "", "");
		return new Result<T>().setCode(resultCode).setMessage(msg);
	}

	/**
	 * 失败
	 */
	public static <T> Result<T> err(IResultCode resultCode, Object ...param) {
		String msg = StringUtil.parseMessage(resultCode.getMessage(), param);
		return new Result<T>().setCode(resultCode).setMessage(msg);
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return this.message;
	}

	public Result<T> setMessage(String message) {
		this.message = message;
		return this;
	}

	public T getData() {
		return data;
	}

	public Result<T> setData(T data) {
		this.data = data;
		return this;
	}

	public String getSvc() {
		return svc;
	}

	public void setSvc(String svc) {
		this.svc = svc;
	}
}