package com.ym.ms.dto;

/**
 * 请求header信息
 *
 * @author zl
 * @date 9.7
 */
public class HeaderInfo {
    /**
     * 用户账户
     */
    private String account;
    /**
     * 用户账户姓名
     */
    private String accountName;
    /**
     * 用户账户uuid
     */
    private String accountUuid;
    /**
     * 组织类型，参考 InsTypeEnum 类型
     */
    /**
     * 组织类型名称
     */
    private String insTypeName;
    /**
     * 组织类型 0平台 1供应商 2分销商 （参考 InsTypeEnum 类型）
     */
    private String insTypeCode;
    /**
     * 机构id
     */
    private String insId;
    /**
     * 机构id
     */
    private String insName;
    /**
     * 组织机构id
     */
    private String orgUuId;
    private String orgName;
    /**
     * 应用id（调用方id）
     */
    private String appId;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccountUuid() {
        return accountUuid;
    }

    public void setAccountUuid(String accountUuid) {
        this.accountUuid = accountUuid;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getInsId() {
        return insId;
    }

    public void setInsId(String insId) {
        this.insId = insId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getInsTypeName() {
        return insTypeName;
    }

    public void setInsTypeName(String insTypeName) {
        this.insTypeName = insTypeName;
    }

    public String getInsName() {
        return insName;
    }

    public void setInsName(String insName) {
        this.insName = insName;
    }

    public String getInsTypeCode()
    {
        return insTypeCode;
    }

    public void setInsTypeCode(String insTypeCode)
    {
        this.insTypeCode = insTypeCode;
    }

    public String getOrgUuId()
    {
        return orgUuId;
    }

    public void setOrgUuId(String orgUuId)
    {
        this.orgUuId = orgUuId;
    }

    public String getOrgName()
    {
        return orgName;
    }

    public void setOrgName(String orgName)
    {
        this.orgName = orgName;
    }

    @Override
    public String toString()
    {
        return "HeaderInfo{" +
                "account='" + account + '\'' +
                ", accountName='" + accountName + '\'' +
                ", accountUuid='" + accountUuid + '\'' +
                ", insTypeName='" + insTypeName + '\'' +
                ", insTypeCode='" + insTypeCode + '\'' +
                ", insId='" + insId + '\'' +
                ", insName='" + insName + '\'' +
                ", orgUuId='" + orgUuId + '\'' +
                ", orgName='" + orgName + '\'' +
                ", appId='" + appId + '\'' +
                '}';
    }
}
