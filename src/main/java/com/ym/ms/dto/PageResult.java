package com.ym.ms.dto;

import com.ym.ms.commons.PaasConstant;
import com.ym.ms.utils.StringUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分面响应结果封装
 *
 * @author zl
 * @date 9.5
 */
public class PageResult<T> implements Serializable {
    private static final long serialVersionUID = 6004532974070436597L;
    private static final String DEFAULT_SUCCESS_MESSAGE = "SUCCESS";

    private String code;

    private String message;

    private long total;

    private List<T> rows;

    private String svc;

    public PageResult() {
    }

    public long getTotal() {
        return total;
    }

    public PageResult<T> setTotal(long total) {
        this.total = total;
        return this;
    }

    public List<T> getRows() {
        return rows;
    }

    public PageResult<T> setRows(List<T> rows) {
        this.rows = rows;
        return this;
    }

    public String getCode() {
        return code;
    }

    public PageResult<T> setCode(String code) {
        this.code = code;
        String serName  = System.getProperties().getProperty("server.name");
        this.svc = serName;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public PageResult<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    /**
     * @Description: 成功的返回结果
     */
    public static <T> PageResult<T> ok() {
        return new PageResult<T>().setCode(PaasConstant.SUCCESS).setMessage(DEFAULT_SUCCESS_MESSAGE).setTotal(0).setRows(new ArrayList<T>());
    }

    /**
     * @Description: 成功的返回结果
     */
    public static <T> PageResult<T> ok(long total, List<T> rows) {
        return new PageResult<T>().setCode(PaasConstant.SUCCESS).setMessage(DEFAULT_SUCCESS_MESSAGE).setTotal(total).setRows(rows);
    }

    /**
     * 失败
     */
    public static <T> PageResult<T> err(String code, String message) {
        String msg = StringUtil.parseMessage(message, "", "", "", "");
        return new PageResult<T>().setCode(code).setMessage(msg);
    }

    /**
     * 失败
     */
    public static <T> PageResult<T> err(String code, String message, Object... param) {
        String msg = StringUtil.parseMessage(message, param);
        return new PageResult<T>().setCode(code).setMessage(msg);
    }

    public String getSvc() {
        return svc;
    }

    public void setSvc(String svc) {
        this.svc = svc;
    }
}
