package com.ym.ms.dto;

/**
 * 请求通用信息（包含header）
 *
 * @author zl
 * @date 9.5
 */
public class RequestContext {

    private final static ThreadLocal<RemoteInfo> holder = new ThreadLocal<RemoteInfo>();

    public static class RemoteInfo {
        private HeaderInfo hi;
        private String ip;

        public HeaderInfo getHi() {
            return hi;
        }

        public void setHi(HeaderInfo hi) {
            this.hi = hi;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }
    }


    public static void setRemoteInfo(RemoteInfo loginInfo) {
        holder.set(loginInfo);
    }


    public static RemoteInfo getRemoteInfo() {
        return holder.get();
    }


    public static void remove() {
        holder.remove();
    }
}
