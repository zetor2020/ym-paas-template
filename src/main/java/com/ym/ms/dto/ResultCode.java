package com.ym.ms.dto;

import com.ym.ms.commons.enums.IResultCode;
import io.swagger.annotations.ApiModelProperty;

/**
 * 响应码枚举，参考HTTP状态码的语义
 * @author zl
 */
public enum ResultCode implements IResultCode {
    /*
     * 业务异常
     */
    BIZ00002("BIZ00002", "业务异常"); // 业务自定义异常


    ResultCode(String code, String msg) {
        this.code = code;
        this.message = msg;
    }

    private String code;

    private String message;

    @Override
    public String getCode() {
        return code;
    }

    private void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    private void setMessage(String msg) {
        this.message = msg;
    }
}