package com.ym.ms.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 结果参数
 *
 * @author zl
 * @date 9.5
 */
@Data
public class TemplateResultDto implements Serializable {
    private static final long serialVersionUID = -663824153556319008L;

    /**
     * 主键
     */
    private Integer id;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createTime;
    /**
     * 创建人
     */
    private String creater;
    /**
     * 更新时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updateTime;
    /**
     * 更新人
     */
    private String updater;

    /**
     * 是否删除
     */
    private Integer isDel;

    private List<KvInfo> insType;


}
