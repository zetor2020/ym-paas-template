package com.ym.ms.dto;

/**
 * 分页
 *
 * @author zl
 * @date 9.5
 */
public class PageRequest {

    private Integer page = 0;
    private Integer pageSize = 10;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
