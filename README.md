# ym-paas-template

---
#### 项目介绍
基于springboot实现后端开发模板程序。

 1. 面向切面打印日志
 2. 支持后端跨域
 3. 使用RestTemplate进行rest请求
 4. ControllerAdvice统一拦截异常
 5. 支持mybatis-plus
 6. 支持Swagger2
 7. 支持profiles切换dev/test/prod分支
 8. 默认使用Interceptor拦截器，拦截Header信息（可使用自定义标签@NonRemoteInfoAnnotate关闭）

#### 快速开始

 - 修改pom.xml中项目名称
 - 修改配置application.yml
 - 启动服务MsApp
 - 首页访问：http://localhost:8080/ym-paas-template/swagger-ui.html
